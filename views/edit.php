<?php
include_once ("../vendor/autoload.php");

use App\Project;

$editobject = new Project();

$editobject->prepare($_GET);
$single = $editobject->show();

if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Update</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->


        <!-- Core JS files -->
        <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/forms/inputs/touchspin.min.js"></script>

        <script type="text/javascript" src="assets/js/core/app.js"></script>
        <script type="text/javascript" src="assets/js/pages/form_input_groups.js"></script>
        <!-- /theme JS files -->

        <!--my custom css styles-->
        <link href="styleforCreate.css" rel="stylesheet" type="text/css">

    </head>
    <body>
        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt=""></a>

                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="sidebar-control sidebar-main-toggle hidden-xs">
                            <i class="icon-paragraph-justify3"></i>
                        </a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/flags/us.png" alt="">
                            <span>English</span>
                            <i class="caret"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#"><img src="assets/images/flags/us.png" alt=""><span>English</span></a></li>
                            <li><a href="#"><img src="assets/images/flags/bd.png" alt=""><span>Bangla</span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="category-content">
                                <div class="media">
                                    <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">PHP Debugger</span>
                                        <div class="text-size-mini text-muted">
                                            <i class="icon-pin text-size-small"></i> &nbsp; BITM, Dhaka
                                        </div>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list">
                                            <li>
                                                <a href="#"><i class="icon-cog3"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->


                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">
                                    <!-- Main -->
                                    <li><a href="../index.php"><i class="icon-people"></i> <span>User Management</span></a></li>
                                    <li class="active"><a href="index_t.php"><i class="icon-point-right"></i> <span>Trainer Management</span></a></li>
                                    <li><a href="#"><i class="icon-books"></i> <span>Course Management</span></a></li>
                                    <li><a href="#"><i class="icon-laptop"></i> <span>Lab Management</span></a></li>
                                    <li><a href="#"><i class="icon-task"></i> <span>Software Management</span></a></li>
                                    <li><a href="#"><i class="icon-pencil5"></i> <span>Assign Course</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->



                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Forms</span> Update</h4>
                            </div>
                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                                <li class="active">Update</li>
                            </ul>


                        </div>
                    </div>
                    <!-- /page header -->




                    <div class="formContent">

                        <legend>Update form</legend>
                        <form action="update.php" method="post">
                            <div class="forms fullName">
                                <label>Enter full name</label>
                                <input type="text" name="full_name" value="<?php echo $single['full_name'] ?>">
                            </div>

                            <div class="forms education">
                                <label>Educational Title</label>
                                <input type="text" name="title">
                            </div>

                            <div class="forms institute">
                                <label>Institution Name</label>
                                <input type="text" name="institute">
                            </div>

                            <div class="forms passYear">
                                <label>Passing year</label>
                                <input type="text" name="year" >
                            </div>

                            <div class="forms team">	
                                <label>Team name</label>
                                <select name="team" required>
                                    <option value="Team1" <?php if ($single['team'] == "Team1") {
    echo "selected";
} ?>>Team 1</option>
                                    <option value="Team2" <?php if ($single['team'] == "Team2") {
    echo "selected";
} ?>>Team 2</option>
                                    <option value="Team3" <?php if ($single['team'] == "Team3") {
    echo "selected";
} ?>>Team 3</option>
                                    <option value="Team4" <?php if ($single['team'] == "Team4") {
    echo "selected";
} ?>>Team 4</option>
                                    <option value="Team5" <?php if ($single['team'] == "Team5") {
    echo "selected";
} ?>>Team 5</option>
                                </select>
                            </div>

                            <div class="forms course">
                                <label>Course Id</label>
                                <select name="courses_id"></select>
                                
                            </div>

                            <div class="forms trainer">
                                <label>Trainer Status</label>
                                <select name="trainer_status">
                                    <option value="leadtrainer" <?php if ($single['trainer_status'] == "leadtrainer") {
    echo "selected";
} ?>>Lead Trainer</option>
                                    <option value="assttrainer" <?php if ($single['trainer_status'] == "assttrainer") {
    echo "selected";
} ?>>Assistant Trainer</option>
                                    <option value="labasst" <?php if ($single['trainer_status'] == "labasst") {
    echo "selected";
} ?>>Lab Assistant</option>
                                </select>
                            </div>

                            <div class="forms phone">
                                <label>Phone</label>
                                <input type="text" name="phone" placeholder="Enter Phone" value="<?php echo $single['phone'] ?>">
                            </div>

                            <div class="forms email">
                                <label>Email</label>
                                <input type="email" name="email" placeholder="Enter Email" value="<?php echo $single['email'] ?>">
                            </div>

                            <div class="forms address1">
                                <label>Address line 1</label>
                                <input type="text" name="add1">
                            </div>

                            <div class="forms address2">
                                <label>Address line 2</label>
                                <input type="text" name="add2">
                            </div>

                            <div class="forms city">
                                <label>City</label>
                                <input type="text" name="city">
                            </div>

                            <div class="forms zip">
                                <label>Zip</label>
                                <input type="text" name="zip" >
                            </div>

                            <div class="forms gender">
                                <label>Select gender</label>
                                <input type="radio" name="gender" value="Male" <?php if ($single['gender'] == "Male") {
    echo "checked";
} ?>/>Male
                                <input type="radio" name="gender" value="Female" <?php if ($single['gender'] == "Female") {
    echo "checked";
} ?>/>Female<br>
                            </div>

                            <div class="forms web">
                                <label>Web Address</label>
                                <input type="text" name="web" value="<?php echo $single['web'] ?>"><br>
                            </div>

                            <div class="forms img">
                                <label>Upload picture</label>
                                <img src="<?php echo "images/" . $mydata['image'] ?>"><br>
                                <input type="file" name="image"/><br>
                                <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>"/><br>
                            </div>
                            <div style="clear:both;"></div>
                            <input type="submit" value="Update">
                        </form>


                        <div class="footer text-muted">
                            &copy; 2015. <a href="#">Trainer Apps</a> by <a target="_blank" href="#">PHP Debugger</a>
                        </div>
                    </div>
                </div>
                <!-- /content wrapper -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
    </body>
</html>