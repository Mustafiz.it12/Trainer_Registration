
<?php
include_once ("../vendor/autoload.php");

use App\Project;

$obj = new Project();
$Alldata = $obj->index();
if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Trainer Managemant Apps - PHP Debugger  </title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/dashboard.js"></script>
	<!-- /theme JS files -->
    <!--my custom css styles-->
    <link href="styleforCreate.css" rel="stylesheet" type="text/css">

</head>

<body>
	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown language-switch">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="assets/images/flags/gb.png" class="position-left" alt="">
						English
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu">
						<li><a class="deutsch"><img src="assets/images/flags/de.png" alt=""> Bangla</a></li>
						<li><a class="english"><img src="assets/images/flags/gb.png" alt=""> English</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">PHP Debugger</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;BITM, Dhaka
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">
								<!-- Main -->
								<li><a href="../index.php"><i class="icon-people"></i> <span>User Management</span></a></li>
                                <li class="active"><a href="index_t.php"><i class="icon-point-right"></i> <span>Trainer Management</span></a></li>
                                <li><a href="#"><i class="icon-books"></i> <span>Course Management</span></a></li>
                                <li><a href="#"><i class="icon-laptop"></i> <span>Lab Management</span></a></li>
                                <li><a href="#"><i class="icon-task"></i> <span>Software Management</span></a></li>
                                <li><a href="#"><i class="icon-pencil5"></i> <span>Assign Course</span></a></li>
							</ul>
						</div>
					</div>
					<!-- /main navigation -->
				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Trainer Management</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Trainer Management</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


                    <div class="main-content">
                        <div class="table-responsive">
                            <table class="table table-bordered" border="1">
                                <tr>
                                    <th>SL</th>
                                    <th>Full Name</th>
                                    <th>Educational Status</th>
                                    <th>Team</th>
                                    <th>Courses Name</th>
                                    <th>Trainer Status</th>
                                    <th>Image</th>

                                    <th colspan="3">Action</th>
                                </tr>
                                <?php
                                $serial = 1;
                                if (isset($Alldata) && !empty($Alldata)) {

                                    foreach ($Alldata as $Singledata) {
                                        ?>

                                        <tr>
                                            <td><?php echo $serial++ ?></td>
                                            <td><?php echo $Singledata['full_name'] ?></td>
                                            <td><?php echo $Singledata['edu_status'] ?></td>
                                            <td><?php echo $Singledata['team'] ?></td>
                                            <td><?php echo $Singledata['title'] ?></td>
                                            <td><?php echo $Singledata['trainer_status'] ?></td>
                                            <td><img src="<?php echo "images/" . $Singledata['image'] ?>" width="200" height="150"></td>
                                            <td><a href="show.php?id=<?php echo $Singledata['id'] ?>"><i class="icon-file-eye"></i>View</a></td>
                                            <td><a href="edit.php?id=<?php echo $Singledata['id'] ?>"><i class="icon-pencil7"></i>Edit</a></td>
                                            <td><a href="delete.php?id=<?php echo $Singledata['id'] ?>"><i class="icon-trash"></i>Remove</a></td>

                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="15">
                                            No available data
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                        <a class="deletelist" href="restore.php">Click to see deleted list</a>
                        <a class="addnew" href="create.php">Add New Trainee</a>
                        
						<div class="footer text-muted">
                            &copy; 2015. <a href="#">Trainer Apps</a> by <a target="_blank" href="#">PHP Debugger</a>
                        </div>
                    </div>
                </div>
                <!-- /content wrapper -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
    </body>
</html>
