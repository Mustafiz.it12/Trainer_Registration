<?php

namespace App;

use PDO;

class Project {

    public $id = '';
    public $dbuser = 'root';
    public $dbpw = 'root';
    public $conn = '';
    public $full_name, $edu_status, $title, $institute, $year, $team, $courses_id, $trainer_status, $image_name,
            $phone, $email, $address, $add1, $add2, $city, $zip, $gender, $web;

    public function __construct() {
        session_start();
        $conn = mysql_connect('localhost', 'root', '') or die('Opps! Unable to connect with Mysql');
        mysql_select_db('lab') or die('Unable to connect with Database');
//        session_start();
//        $this->conn = new PDO('mysql:host=localhost;dbname=lab', $this->dbuser, $this->dbpw);
    }

    public function prepare($data = '') {
        if (array_key_exists('full_name', $data) && !empty($data['full_name'])) {
            $this->full_name = $data['full_name'];
        } else {
            $_SESSION['fnemty'] = "Full Name required";
        }
        if (!empty($data['title'])) {
            $this->title = $data['title'];
        } else {
            $_SESSION['temty'] = "Title required";
        }

        if (!empty($data['institute'])) {
            $this->institute = $data['institute'];
        }
        if (!empty($data['year'])) {
            $this->year = $data['year'];
        }
        $arr = array("$this->title", "$this->institute", "$this->year");
        $this->edu_status = implode(".", $arr);
        if (!empty($data['team']) && isset($data['team'])) {
            $this->team = $data['team'];
        } else {
            $_SESSION['teamemty'] = "Team required";
        }
        if (!empty($data['courses_id'])) {
            $this->courses_id = $data['courses_id'];
        }
        if (!empty($data['trainer_status'])) {
            $this->trainer_status = $data['trainer_status'];
        }
        if (!empty($data['phone'])) {
            $this->phone = $data['phone'];
        } else {
            $_SESSION['pemty'] = "Phone required";
        }
        if (!empty($data['email'])) {
            $this->email = $data['email'];
        } else {
            $_SESSION['eemty'] = "Email required";
        }
        if (!empty($data['add1'])) {
            $this->add1 = $data['add1'];
        } else {
            $_SESSION['a1emty'] = "Address required";
        }
        if (!empty($data['add2'])) {
            $this->add2 = $data['add2'];
        } else {
            $_SESSION['a2emty'] = "Address required";
        }
        if (!empty($data['city'])) {
            $this->city = $data['city'];
        } else {
            $_SESSION['a3emty'] = "City required";
        }
        if (!empty($data['zip'])) {
            $this->zip = $data['zip'];
        } else {
            $_SESSION['a4emty'] = "Zip required";
        }
        $arr1 = array("$this->add1", "$this->add2", "$this->city", "$this->zip");
        $this->address = implode(" ", $arr1);
        if (!empty($data['web'])) {
            $this->web = $data['web'];
        } 
        if (!empty($data['gender'])) {
            $this->gender = $data['gender'];
        }
        if (!empty($data['image'])) {
            $this->image_name = $data['image'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        $_SESSION['Form'] = $data;
        return $this;
    }

    public function store() {

        if (!empty($this->full_name)) {
            $query = "INSERT INTO `lab`.`trainers` (`id`, `unique_id`, `full_name`, `edu_status`, `team`, `courses_id`, `trainer_status`, `image`, `phone`, `email`, `address`, `gender`, `web`, `created`) VALUES (NULL, '" . uniqid() . "', '$this->full_name', '$this->edu_status', '$this->team', '$this->courses_id', '$this->trainer_status', '$this->image_name', '$this->phone', '$this->email', '$this->address', '$this->gender', '$this->web', '" . date('Y-m-d h:m:s') . "' )";
//             echo $query;
//                     die();
            if (mysql_query($query)) {
                $_SESSION['Message'] = "Data Successfully Submitted";
                header('location:create.php');
            }
        } else {
            header('location:create.php');
        }
//        try {
//
//            $query = "INSERT INTO trainers(id, unique_id, full_name, edu_status, team, courses_id, trainer_status, phone, email, address, gender, web, created) VALUES(:i, :u, :fn, :ed, :tm, :c, :ts, :ph, :em, :ad, :gn, :web, :cr)";
//            $stmt = $this->conn->prepare($query);
//            $stmt->execute(array(
//                ':i' => null,
//                ':u' => uniqid(),
//                ':fn' => $this->full_name,
//                ':ed' => $this->edu_status,
//                ':tm' => $this->team,
//                ':c' => $this->courses_id,
//                ':ts' => $this->trainer_status,
//                ':ph' => $this->phone,
//                ':em' => $this->email,
//                ':ad' => $this->address,
//                ':gn' => $this->gender,
//                ':web' => $this->web,
//                ':m' => date('Y-m-d h:m:s')));
//            header('location:create.php');
//        } catch (PDOException $e) {
//            echo 'Error: ' . $e->getMessage();
//        }
    }

    public function index() {
        $qr = "SELECT * FROM trainers INNER JOIN courses ON trainers.courses_id = courses.id where is_delete= 0";
//        $qr = "SELECT * FROM `trainers` where is_delete= 0";
        $mydata = mysql_query($qr);
        while ($row = mysql_fetch_assoc($mydata)) {
            $this->data[] = $row;
        }
        $_SESSION['Form'];
        return $this->data;
    }

    public function trash() {
        $qr1 = "SELECT * FROM `trainers` where is_delete= 1";
        $mydata = mysql_query($qr1);
        while ($row = mysql_fetch_assoc($mydata)) {
            $this->data[] = $row;
        }
        $_SESSION['Form'];
        return $this->data;
    }

    public function show() {
        $query = "SELECT * FROM trainers LEFT JOIN courses ON trainers.courses_id = courses.id WHERE trainers.courses_id= ".$this->id ;
        $mydata = mysql_query($query);
        $row = mysql_fetch_assoc($mydata);
        return $row;
    }
    public function Allcourse() {

        $query = "SELECT id, title FROM `courses`";
        $mydata = mysql_query($query);
        while ($row = mysql_fetch_assoc($mydata)) {
            $this->data[] = $row;
        }
        return $this->data;
    }

    public function delete() {
        $query = "UPDATE `lab`.`trainers` SET is_delete = 1, deleted = '" . date('Y-m-d h:m:s') . "' WHERE `trainers`.`courses_id` =" . "'" . $this->id . "'";
//        $query = "DELETE FROM `php26`.`mobiles` WHERE `mobiles`.`unique_id` =" . "'" . $this->id . "'";

        if (mysql_query($query)) {
            $_SESSION['Message'] = "Trainer Successfully Removed";
        }
        header("location:index_t.php");
    }

    public function restore() {
        $query = "UPDATE `lab`.`trainers` SET is_delete = 0 WHERE `trainers`.`id` =" . "'" . $this->id . "'";
//        $query = "DELETE FROM `php26`.`mobiles` WHERE `mobiles`.`unique_id` =" . "'" . $this->id . "'";
        if (mysql_query($query)) {
            $_SESSION['Message'] = "Trainer Successfully restored";
        }
        header("location:index_t.php");
    }

    public function update() {
        $query = "UPDATE `lab`.`trainers` SET full_name = '$this->full_name', edu_status = '$this->edu_status', team = '$this->team', courses_id = '', trainer_status = '$this->trainer_status', phone = '$this->phone', email = '$this->email', address = '$this->address', gender = '$this->gender', web = '$this->web', updated = '" . date('Y-m-d h:m:s') . "' WHERE `trainers`.`unique_id` =" . "'" . $this->id . "'";
//        $query = "UPDATE `lab`.`trainers` SET full_name = '$this->full_name', edu_status = '$this->edu_status', team = '$this->team', courses_id = '$this->courses_id', trainer_status = '$this->trainer_status', phone = '$this->phone', email = '$this->email', address = '$this->address', gender = '$this->gender', web = '$this->web', modified = '" . date('Y-m-d h:m:s') . "' WHERE `trainers`.`unique_id` =" . "'" . $this->id . "'";
//                          
        if (mysql_query($query)) {
            $_SESSION['Message'] = "Data Successfully Updated";
        }
        header("location:edit.php?id=$this->id");
    }

//
//    public function show() {
//
//        $qu = "SELECT * FROM signup WHERE unique_id=" . "'" . $this->id . "'";
//        $query = $this->conn->prepare($qu);
//
//        $query->execute();
//        $result = $query->fetch(PDO::FETCH_ASSOC);
////$_SESSION['Message'] = "Data Successfully updated";
//        return $result;
//    }
//
//    public function update() {
//        $query = "UPDATE mobiles SET title= :ti, laptop = :lp WHERE unique_id = :id";
//        $stmt = $this->conn->prepare($query);
//        $stmt->execute(
//                array(
//                    ':ti' => $this->title,
//                    ':lp' => $this->laptop,
//                    ':id' => $this->id,
//        ));
//        header('location:index.php');
//    }
//
//    public function delete() {
//        $qu = "DELETE FROM signup WHERE unique_id =" . "'" . $this->id . "'";
//        $this->conn->exec($qu);
////        $query = $this->conn->prepare($qr);
////        $query->execute();
////        $result = $query->fetch(PDO:: FETCH_ASSOC);
////        if (mysql_query($query)) {
////            $_SESSION['Message'] = "Data Successfully Deleted";
////        }
//        header("location:index.php");
//    }
}
